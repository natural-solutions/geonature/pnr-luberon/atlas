# Installation

Tout d'abord, il est nécessaire de cloner ce projet : 
```bash
git clone https://gitlab.com/natural-solutions/geonature/pnr-luberon/atlas.git
```

Ensuite se placer dans le dossier où est situé le fichier
`docker-compose.yml`. Ceci sera pris comme hypothèse pour les commandes
suivantes.



## Développement

```bash
docker-compose up -d
```

## Production

```bash
docker-compose up -d atlas-nginx
```

Par défaut, l'application est servie sur le port 8000

# Gérer l'application en production
## Relancer l'application après modification des données
```bash
docker-compose build --no-cache atlas-nginx
docker-compose up -d atlas-nginx 
```

Il n'est pas obligatoire d'éteindre l'application après avoir lancé le build,
docker-compose se chargera de remonter la nouvelle image dans le conteneur.

## Eteindre l'application 
```bash
docker-compose down
```

# Configuration

La configuration de l'atlas s'effectue via le fichier `data/config.yml`. La
documentation de se fichier est disponible sur la documentation de l'outil
disponible
[ici](https://natural-solutions.gitlab.io/geonature/zones-humides/atlas/configuration/#fichier-du-configuration)


# Explications

## Fichiers
2 fichiers Dockerfile sont présents dans ce dépôt :
- `nginx.dockerfile` permet une compilation de l'atlas en un dossier public
  ensuite servi par nginx
- `node.dockerfile` sert au développement : lance un serveur de développement

Le fichier `.gitlab-ci.yml` permet le déploiement continue et est à ignorer pour
une installation en local

Le fichier `docker-compose.yml` permet de lancer les conteneurs comme décrit
précédemment

Le dossier `public` contient tous les fichiers de customisation de l'atlas.

## Lancement
Au lancement, docker-compose va ordonner la construction des images docker.
Ces images, se basent sur les images du projet suivant : 
[Atlas Zones
Humides](https://gitlab.com/natural-solutions/geonature/zones-humides/atlas)

Et ajoute le dossier data contenant notamment le fichier `config.yml`

